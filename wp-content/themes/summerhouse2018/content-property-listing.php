<?php
// Get pages by category
	$args = array('category' => 4, 'post_type' =>  'page'); 
    $propList = get_posts($args);
    foreach ($propList as $page) {
	    echo $page->category;
		$propPage = $page->ID;
		$cats = get_the_category($propPage);
		if ($cats[0]->cat_ID == 4){
			$field_data = CFS()->get(false, $propPage);
			$propLink = get_permalink($propPage);
			$soldText = "THIS PROPERTY IS NOW SOLD";
			?>		
		 <div class="col-md-6 item-grid__container">
	        <div class="listing">
	          <div class="item-grid__image-container">
	            <a href="<?php echo $propLink ?>">
	              <div class="item-grid__image-overlay"></div><!-- .item-grid__image-overlay -->
	              <?php 
		              
		              $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($propPage),array(555, 414));
		              if(isset($feat_image)){?>
			              <img src="<?php echo $feat_image[0] ?>" alt="<?php echo $page->post_title ?>" class="listing__img">
		              <?php } ?>
	              <!--<img src="<?php echo $field_data['property_main_image'] ?>" alt="<?php echo $page->post_title ?>" class="listing__img">-->
	              <!--<span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>-->
	            </a>
	          </div><!-- .item-grid__image-container -->
	          <?php if ($field_data['property_available'] >0){?>
			  <div class="item-grid__content-container">
	            <div class="listing__content">
	              <div class="listing__header">
	                <div class="listing__header-primary">
	                  <h3 class="listing__title"><a href="<?php echo $propLink ?>"><?php echo $page->post_title ?></a></h3>
	                  <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> <?php echo $field_data['property_location'] ?></p>
	                </div><!-- .listing__header-primary -->
	                <p class="listing__price"><?php echo $field_data['property_price'] ?></p>
	              </div><!-- .listing__header -->
	              <div class="listing__details">
	                <ul class="listing__stats">
	                  <!--<li><span class="listing__figure"><?php echo $field_data['property_number'] ?></span> Homes</li>-->
	                  <li><span class="listing__figure"><?php echo $field_data['property_beds'] ?></span> Beds</li>
	                </ul><!-- .listing__stats -->
	                <a href="<?php echo $propLink ?>" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
	              </div><!-- .listing__details -->
	            </div><!-- .listing-content -->
	          </div><!-- .item-grid__content-container -->
	        </div><!-- .listing -->
	      </div><!-- .col -->
	      <?php 
		   		} else {?>
				  <div class="item-grid__content-container">
		            <div class="listing__content">
		              <div class="listing__header">
		                <div class="listing__header-primary">
		                  <h3 class="listing__title"><a href="<?php echo $propLink ?>"><?php echo $page->post_title ?></a></h3>
		                  <h3 class="listing__title"><a href="<?php echo $propLink ?>"><?php echo $soldText ?></a></h3>
		                  <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> <?php echo $field_data['property_location'] ?></p>
		                </div><!-- .listing__header-primary -->
		              <!--  <p class="listing__price"><?php echo $field_data['property_price_from'] ?></p>-->
		              </div><!-- .listing__header -->
		              <div class="listing__details">
		                <ul class="listing__stats">
		                <!--  <li><span class="listing__figure"><?php echo $field_data['property_number'] ?></span> Homes</li>
		                  <li><span class="listing__figure"><?php echo $field_data['property_beds'] ?></span> Beds</li>-->
		                </ul><!-- .listing__stats -->
		               <!-- <a href="<?php echo $propLink ?>" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>-->
		              </div><!-- .listing__details -->
		            </div><!-- .listing-content -->
		          </div><!-- .item-grid__content-container -->
		        </div><!-- .listing -->
		      </div><!-- .col --> 
		  <?php
		  }
	    }
	}?>



