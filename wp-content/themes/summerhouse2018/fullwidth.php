<?php /* Template Name: Full-width content Template */ ?>
<?php get_header(); ?>
<section class="about-us">
  <div class="container">
<?php custom_breadcrumbs(); ?>
    <div class="about-us__main">
      <div class="row">
        <main class="col-md-12 col-md-main">
          <div class="about-us__img">
            <?php if ( has_post_thumbnail() ) { ?>
					<?php $img_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'full'); ?>
					<img src="<?php echo $img_url[0];?>" alt="" class="img-responsive"/>
			<?php } ?>
          </div><!-- .about-us__img -->
          <h1 class="about-us__title"><?php the_title(); ?></h1>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
					 print the_content();
				endwhile; else:
					print '<p>Sorry, no posts matched your criteria.</p>';
				endif; ?>        
		</main><!-- .col -->
      </div><!-- .row -->
    </div><!-- .about-us__main -->
  </div><!-- .container -->
</section><!-- .about-us -->
<?php get_footer(); ?>