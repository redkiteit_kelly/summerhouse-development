<?php /* Template Name: Generic content Template */ ?>
<?php get_header(); ?>
<section class="about-us">
  <div class="container">
<?php custom_breadcrumbs(); ?>
    <div class="about-us__main">
      <div class="row">
        <main class="col-md-8 col-md-main">
          <div class="about-us__img">
            <?php if ( has_post_thumbnail() ) { ?>
				<?php $img_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'full'); ?>
				<img src="<?php echo $img_url[0];?>" alt="" class="img-responsive"/>
			<?php } ?>
          </div>
          <h1 class="about-us__title"><?php the_title(); ?></h1>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
				 print the_content();
			endwhile; else:
				print '<p>Sorry, no posts matched your criteria.</p>';
			endif; ?>        
		</main>
        <aside class="col-md-4 col-md-sidebar">
	        <?php if (is_page(79)){?>
	          <section class="widget">
	            <form class="contact-form contact-form--bordered contact-form--wild-sand">
	              <div class="contact-form__header">
	                <h3 class="contact-form__title">Make an enquiry</h3>
	              </div><!-- .contact-form__header -->
	              <div class="contact-form__body">
	                  <?php 
		              $contact_form = CFS()->get('contact_form');
		              echo $contact_form;
		              ?>
	              <div class="contact-form__footer">
	              </div><!-- .contact-form__footer -->
	            </form><!-- .contact-form -->
	          </section><!-- .widget -->
	      <?php  } ?>
        </aside><!-- .col -->
      </div><!-- .row -->
    </div><!-- .about-us__main -->
  </div><!-- .container -->
</section><!-- .about-us -->
<?php get_footer(); ?>