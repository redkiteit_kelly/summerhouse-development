<?php /* Template Name: Home Template */ ?>
<?php get_header(); ?>
<section class="main-slider">
	<div class="container">
	  	<div class="row">
	        <div class="col-md-12">
			<?php putRevSlider("homeslider", "homepage");?>
	        </div>
	  	</div>
	</div>
	<section class="main-search main-search--absolute">
		<div class="container">
			<div class="main-search__container">
				<section class="listing-search">
					<?php 
					if ( have_posts() ) : while ( have_posts() ) : the_post();			  	
						get_template_part( 'content-slide-strap', get_post_format() );
					endwhile; endif; 
					?>
				</section>
			</div><!-- .main-search__container -->
		</div><!-- .container -->
	</section><!-- .main-search -->
</section><!-- .main-slider -->

<section class="item-grid">
  <div class="container">
    <div class="row">
		<?php 
		if ( have_posts() ) : while ( have_posts() ) : the_post();			  	
			get_template_part( 'content-location-listing', get_post_format() );
		endwhile; endif; 
						?>
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .item-grid-2 -->
<section class="features">
  <div class="features__overlay">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="feature">
            <img src="<?php bloginfo( 'template_url' ); ?>/images/icon_map.png" alt="Map" class="feature__icon">
            <h3 class="feature__title">Popular Locations</h3>
            <p class="feature__desc">A selection of great village locations near schools, shops and commuter links</p>
          </div><!-- .feature -->
        </div><!-- .col -->

        <div class="col-sm-4">
          <div class="feature">
            <img src="<?php bloginfo( 'template_url' ); ?>/images/icon_search.png" alt="Search" class="feature__icon">
            <h3 class="feature__title">Homes now available</h3>
            <p class="feature__desc">Reserve your new home now and move in, in 2018</p>
          </div><!-- .feature -->
        </div><!-- .col -->
        <div class="col-sm-4">
          <div class="feature">
            <img src="<?php bloginfo( 'template_url' ); ?>/images/icon_negotiation.png" alt="Negotiation" class="feature__icon">
            <h3 class="feature__title">Buy now with a 5% deposit</h3>
            <p class="feature__desc">The government-funded Help to Buy Equity Loan is available until 2021</p>
          </div><!-- .feature -->
        </div><!-- .col -->  
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .features__overlay -->
</section><!-- .features -->
<section class="gallery">
	<!-- Fill width -->
	<div class="container">
		<div class="row">
			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();			  	
					get_template_part( 'content-gallery', get_post_format() );
				endwhile; endif; 
			?>
		</div>
	</div>
</section>
<section class="map">
	<div class="container">
		<div class="row">
			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					get_template_part( 'content-map', get_post_format() );  
				endwhile; endif;
			?>
		</div>
	</div>
</section>
<?php get_footer(); ?>