<?php
/**
 * Realand Real Estate functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 */

//Register custom nav walker for Bootstrap
require_once("_inc/bootstrap_navwalker.inc.php");

/**
 * Realand Real Estate only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'realand_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 */
function realand_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/realand
	 * If you're building a theme based on realand, use a find and replace
	 * to change 'realand' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'realand' );
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu'),
		'social'  => __( 'Social Links Menu'),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );
	add_theme_support( 'post-thumbnails', array( 'page' ) );   

	/*
	 * Enable support for custom logo.
	 *
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 217
	) );

	if ( function_exists('register_sidebar') ) {
		register_sidebar(array('name' => 'Footer Left Widget',
		'before_title'=>'<h3 class="f-title">', 'after_title'=>'</h3>', 
		'before_widget'=>'<div id="%1$s" class="widget %2$s">', 'after_widget'=>'</div>'
		));
		register_sidebar(array('name' => 'Footer Centre Widget',
		'before_title'=>'<h3 class="f-title">', 'after_title'=>'</h3>', 
		'before_widget'=>'<div id="%1$s" class="widget %2$s">', 'after_widget'=>'</div>'
		));
		register_sidebar(array('name' => 'Footer Right Widget',
		'before_title'=>'<h3 class="f-title">', 'after_title'=>'</h3>', 
		'before_widget'=>'<div id="%1$s" class="widget %2$s">', 'after_widget'=>'</div>'
		));
		register_sidebar(array('name' => 'News Ticker',
		'before_title'=>'<h3 class="f-title">', 'after_title'=>'</h3>', 
		'before_widget'=>'<div id="%1$s" class="widget %2$s">', 'after_widget'=>'</div>'
		));
	}
}
endif; // realand_setup
add_action( 'after_setup_theme', 'realand_setup' );


// Remove wysiwyg editor from edit screen

/*add_action('init', 'init_remove_support',100);
function init_remove_support(){
    $post_type = 'page';
    remove_post_type_support( $post_type, 'editor');
}*/

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Realand 2017 1.1
 */
function realand_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'realand_javascript_detection', 0 );


// Load favicons
add_action('wp_head', function() {
	/*echo '<link rel="shortcut icon" href="assets/favicon/favicon.ico">';
	echo '<link rel="apple-touch-icon" href="assets/favicon/apple-touch-icon.png">';
	echo '<link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-touch-icon-72x72.png">';
	echo '<link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-touch-icon-114x114.png">';
	echo '<link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-touch-icon-144x144.png">';*/
});

/**
 * Enqueue scripts and styles.
 *

 */
function realand_scripts() {	
	wp_enqueue_style( 'realand-fonts-open', '//fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:700,800|Roboto:400,500,700"', array(), false );
	wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/css/plugins.css');
	wp_enqueue_style( 'realand-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'realand_scripts' );


// add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="'. get_template_directory_uri() . '/assets/js/html5shiv.js"></script>';
	echo '<script src="'. get_template_directory_uri() . '/assets/js/respond.min.js"></script>';      
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');


function realand_jscripts() {
	wp_deregister_script( 'jquery' );	
	wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-1.12.4.min.js', array(), '2.2.2', true);
	wp_enqueue_script('plugins', get_template_directory_uri() . '/js/plugins.js', array(), '', true);
	wp_enqueue_script('custom-js', get_template_directory_uri() . '/js/custom.js', array(), '', true);
	wp_enqueue_script('fontawesome', 'https://use.fontawesome.com/releases/v5.0.6/js/all.js', array(), '', true);
	//wp_enqueue_script('google-maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4', array(), '1.0', true);
	//wp_enqueue_script('mopas-infobox', '//cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js', array(), '1.0', true);
	
}
add_action('wp_enqueue_scripts', 'realand_jscripts' );

// Force infobox to load after google maps plugin scripts
function realand_infobox(){
	//wp_enqueue_script('mopas-infobox', '//cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js', array(), '1.0', true);
}

add_action('wp_enqueue_scripts', 'realand_infobox', 999999);
/**
 * Add preconnect for Google Fonts.
 *
 * @since Realand 2017 1.7
 *
 * @param array   $urls          URLs to print for resource hints.
 * @param string  $relation_type The relation type the URLs are printed.
 * @return array URLs to print for resource hints.
 */
function realand_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'realand-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '>=' ) ) {
			$urls[] = array(
				'href' => 'https://fonts.gstatic.com',
				'crossorigin',
			);
		} else {
			$urls[] = 'https://fonts.gstatic.com';
		}
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'realand_resource_hints', 10, 2 );

function realand_testimonials_post_type() {
	register_post_type( 'testimonial-post',
		array(
			  'labels' => array(
			  'name' => __( 'Testimonials' ),
			  'singular_name' => __( 'Testimonial' ),
			  'add_new' => __( 'Add New' ),
		   	  'add_new_item' => __( 'Add New Testimonial' ),
			  'edit' => __( 'Edit' ),
	  		  'edit_item' => __( 'Edit Testimonial' ),
	          'new_item' => __( 'New Testimonial' ),
			  'view' => __( 'View Testimonials' ),
			  'view_item' => __( 'View Testimonial' ),
			  'search_items' => __( 'Search Testimonials' ),
	  		  'not_found' => __( 'No Testimonials found' ),
	  		  'not_found_in_trash' => __( 'No Testimonials found in Trash' ),
			  'parent' => __( 'Parent Testimonials' ),
			),
			'public' => true,
			'menu_icon' => 'dashicons-format-quote',
			'supports' => array( 
				'title', 
				'editor',
				'custom-fields'
				),
		)
	);
}
add_action( 'init', 'realand_testimonials_post_type' );


// Breadcrumbs
function custom_breadcrumbs($div=false) {
       
    // Settings
    $separator          = '&gt;';
    $breadcrumbs_id      = 'breadcrumbs';
    $breadcrumbs_class   = 'ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
        // Build the breadcrumbs
        echo '<ul id="' . $breadcrumbs_id . '" class="' . $breadcrumbs_class . '">';
        if($div){
	        echo '<div class="container">';
        }
        // Home page
        echo '<li class="ht-breadcrumbs__item"><a class="ht-breadcrumbs__link" href="' . get_home_url() . '" title="' . $home_title . '"><span class="ht-breadcrumbs__title">' . $home_title . '</a></span></li>';
                   
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="ht-breadcrumbs__item"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__title"><a class="ht-breadcrumbs__link" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></span></li>';
                
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="ht-breadcrumbs__item"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__title"><a class="ht-breadcrumbs__link" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></span></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="ht-breadcrumbs__item">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="ht-breadcrumbs__item"><strong class="ht-breadcrumbs__title" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__title"><a class="ht-breadcrumbs__link" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></span></li>';
                //echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="ht-breadcrumbs__item"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="ht-breadcrumbs__item"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="ht-breadcrumbs__item"><strong class="ht-breadcrumbs__title">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__title"><a class="ht-breadcrumbs__link" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></span></li>';
                    //$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__page" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__page" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } /*elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
      */ 
      	if($div){
	      	echo '</div>';
      	}
        echo '</ul>';
           
    }
       
}