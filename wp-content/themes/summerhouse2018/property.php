<?php /* Template Name: Property Template */ ?>
<?php get_header(); ?>
<section class="property">
	<?php custom_breadcrumbs(true); ?>

    <div class="property__container">
      
        <div class="property__placeholder">
	        <div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			$prop_slide = CFS()->get('property_slideshow'); 
			if(!isset($prop_slide)){
				if ( has_post_thumbnail() ) {
					echo the_post_thumbnail( array(1170, 700) );
				}
			} else {
				echo $prop_slide;
			}
		endwhile; endif;
	?></div><!-- #property-map -->
      </div><!-- .property__placeholder -->

      <div class="container">
        <div class="row">
          <aside class="col-md-3">
            <div class="widget__container">
              <section class="widget">
                <div class="contact-form contact-form--white">
                  <div class="contact-form__header">
                    <div class="contact-form__header-container">
                      <div class="contact-info">
                        <h3 class="contact-name">Summerhouse Development</h3>
                        <a href="tel:<?php echo CFS()->get('contact_phone_number'); ?>" class="contact-number">Call: <?php echo CFS()->get('contact_phone_number'); ?></a>
                      </div><!-- .contact-info -->
                    </div><!-- .contact-form__header-container -->
                  </div><!-- .contact-form__header -->
                  
                  <div class="contact-form__body">
	              <?php $contact_form = CFS()->get('property_contact_form');
	              echo $contact_form; ?>
                  </div> 
                  <a href="http://www.marcusalderson.co.uk" title="Marcus Alderson Estate Agents" target="_blank"><img src="/wp-content/themes/summerhouse2018/images/marcus-alderson-logo.png" alt="" /></a><br/>
				  <a href="http://www.forceshomes.com" title="Foreces Homes" target="_blank"><img src="/wp-content/themes/summerhouse2018/images/forces-homes-logo.png" alt="" /></a>
              </section><!-- .widget -->
			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();			  	
					get_template_part( 'content-similar-properties', get_post_format() );
				endwhile; endif; 
			?>
			              
            </div><!-- .widget__container -->
          </aside>
        
          <main class="col-md-9">
            <div class="property__feature-container">
              <div class="property__header property__header--v3">
                <div class="property__header-container">
                  <div class="property__primary">
	                  <div class="clear20"></div>
                      <h2 class="property__name"><?php the_title(); ?></h2>
                      <span class="property__address"><i class="ion-ios-location-outline property__address-icon"></i><?php echo CFS()->get('property_location'); ?></span>
                    </div><!-- .property__title -->

                    <div class="property__details property__main-item">
                      <ul class="property__stats">
                        <li class="property__stat"><span class="property__figure"><?php echo CFS()->get('property_beds'); ?><sup>&plus;</sup></span> Beds</li>
                        <li class="property__stat"><span class="property__figure"><?php echo CFS()->get('property_bathrooms'); ?></span> Bathrooms</li>
                      </ul><!-- .listing__stats -->
                    </div><!-- .property__details -->
                  </div><!-- .property__primary -->

                  <div class="property__secondary">
                    <div class="property__price property__price--b-margin">
                      <h4 class="property__price-primary"><?php echo CFS()->get('property_price'); ?></h4>
                    </div><!-- property__price -->
                  </div><!-- .property__secondary -->
                </div><!-- .property__header-container -->
              </div><!-- .property__header -->

              <div class="property__feature">
                <h3 class="property__feature-title property__feature-title--b-spacing">Property Description</h3>
                <?php echo CFS()->get('property_description'); ?>
              </div><!-- .property__feature -->

              <div class="property__feature">
                <h3 class="property__feature-title property__feature-title--b-spacing">Property Details</h3>
                <ul class="property__details-list">
	               <?php $deet_block = CFS()->get('property_details');
					   foreach ($deet_block as $deet) {	
						   $deetEx = explode(" – ", $deet['detail']);
						   $deetType = $deetEx[0];
						   $deetName = $deetEx[1];
					   ?>
                  <li class="property__details-item"><span class="property__details-item--cat"><?php echo $deetType ?>:</span> <?php echo $deetName ?></li>
                  <?php } ?>
                </ul><!-- .property__details-list -->
              </div><!-- .property__feature -->

              <div class="property__feature">
                <h3 class="property__feature-title property__feature-title--b-spacing">Property Features</h3>
                <ul class="property__features-list">
	               <?php $feat_block = CFS()->get('property_features');
					   foreach ($feat_block as $feat) {	?>
                  <li class="property__features-item"><span class="property__features-icon ion-checkmark-round"></span><?php echo $feat['feature'] ?></li>
                  <?php } ?>
                </ul><!-- .property__features-list -->
              </div><!-- .property__feature -->

              <div class="property__feature">
	              <?php 
		              $prop_floors = CFS()->get('property_floor_plans');
		              if (count($prop_floors)>0) {
	              ?>
                <h3 class="property__feature-title property__feature-title--b-spacing">Floor Plans (<?php echo count($prop_floors) ?>)</h3>
                <?php foreach ($prop_floors as $floor) {?>
	                <div class="property__accordion">
	                  <div class="property__accordion-header">
	                    <div class="property__accordion-textcontent">
	                      <span class="property__accordion-title"><?php echo $floor['floor_name'] ?></span>
	                      <ul class="property__accordion-stats">
	                        <li class="property__accordion-figure"><span class="property__accordion-figure--cat"><?php echo $floor['floor_details'] ?></span></li>
	                      </ul><!-- .property__accordion-stats -->
	                    </div><!-- .property__accordion-textcontent -->
	                    <i class="fa fa-caret-up property__accordion-expand" aria-hidden="true"></i> 
	                  </div><!-- .property__accordion-header -->
	                  <div class="property__accordion-content property__accordion-content--active">
	                    <img src="<?php echo $floor['floor_image'] ?>" alt="Floor Plan">
	                  </div><!-- .property__accordion-content -->
	                </div><!-- .property__accordion -->
                <?php }
	                } ?>
          </main>
        </div><!-- .row -->
      </div><!-- .container -->
    </div><!-- .property__container -->
</section><!-- .property -->

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="pswp__bg"></div><!-- .pswp__bg -->
  <div class="pswp__scroll-wrap">
    <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
    </div><!-- pswp__container -->

    <div class="pswp__ui pswp__ui--hidden"> 
      <div class="pswp__top-bar">
        <div class="pswp__counter"></div><!-- .pswp__counter -->

        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

        <div class="pswp__preloader">
          <div class="pswp__preloader__icn">
            <div class="pswp__preloader__cut">
              <div class="pswp__preloader__donut"></div><!-- .pswp__preloader__donut -->
            </div><!-- .pswp__preloader__cut -->
          </div><!-- .pswp__preloader__icon -->
        </div><!-- pswp__preloader -->
      </div><!-- .pswp__top-bar -->

      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><!-- .pswp__button -->
      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><!-- .pswp__button -->

      <div class="pswp__caption">
        <div class="pswp__caption__center"></div><!-- .pswp__caption__center -->
      </div><!-- .pswp__caption -->
    </div><!-- .pswp__ui -->
  </div><!-- .pswp__scroll-wrap -->
</div><!-- .pswp -->

<?php get_footer(); ?>