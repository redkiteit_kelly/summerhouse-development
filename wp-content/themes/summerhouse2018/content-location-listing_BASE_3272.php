<?php
// Get pages by category
	$args = array('category' => 3, 'post_type' =>  'page'); 
    $locList = get_posts($args);
    foreach ($locList as $page) {
	    echo $page->category;
		$locPage = $page->ID;
		$cats = get_the_category($locPage);
		if ($cats[0]->cat_ID == 3){
			$field_data = CFS()->get(false, $locPage);
			$locLink = get_permalink($locPage);
			$soldText = "ALL PROPERTIES NOW SOLD";
			$comingsoonText = "SITE ACQUIRED - COMING SOON";?>		
		 <div class="col-md-6 item-grid__container">
	        <div class="listing">
	          <div class="item-grid__image-container">
	            <a href="<?php echo $locLink ?>">
	              <div class="item-grid__image-overlay"></div><!-- .item-grid__image-overlay -->
	              <img src="<?php echo $field_data['location_image'] ?>" alt="<?php echo $page->post_title ?>" class="listing__img">
	              <!--<span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>-->
	            </a>
	          </div><!-- .item-grid__image-container -->
	          <?php if ($field_data['location_available']['Yes'] == "Yes"){?>
			  <div class="item-grid__content-container">
	            <div class="listing__content">
	              <div class="listing__header">
	                <div class="listing__header-primary">
	                  <h3 class="listing__title"><a href="<?php echo $locLink ?>"><?php echo $field_data['location_name'] ?></a></h3>
	                  <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> <?php echo $field_data['location_location'] ?></p>
	                </div><!-- .listing__header-primary -->
	                <p class="listing__price"><?php echo $field_data['location_price_from'] ?></p>
	              </div><!-- .listing__header -->
	              <div class="listing__details">
	                <ul class="listing__stats">
	                  <li><span class="listing__figure"><?php echo $field_data['location_number'] ?></span> Homes</li>
	                  <li><span class="listing__figure"><?php echo $field_data['location_beds'] ?></span> Beds</li>
	                </ul><!-- .listing__stats -->
	                <a href="<?php echo $locLink ?>" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
	              </div><!-- .listing__details -->
	            </div><!-- .listing-content -->
	          </div><!-- .item-grid__content-container -->
	        </div><!-- .listing -->
	      </div><!-- .col -->
	      <?php } else if ($field_data['location_available']['Site Acquired - Coming soon'] == "Yes") {?>
				  <div class="item-grid__content-container">
		            <div class="listing__content">
		              <div class="listing__header">
		                <div class="listing__header-primary">
		                  <h3 class="listing__title"><a href="<?php echo $locLink ?>"><?php echo $field_data['location_name'] ?></a></h3>
		                  <h3 class="listing__title"><a href="<?php echo $locLink ?>"><?php echo $comingsoonText ?></a></h3>
		                  <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> <?php echo $field_data['location_location'] ?></p>
		                </div>
		              </div><!-- .listing__header -->
		            </div><!-- .listing-content -->
		          </div><!-- .item-grid__content-container -->
		        </div><!-- .listing -->
		      </div><!-- .col --> 
		  <?php } else {?>
				  <div class="item-grid__content-container">
		            <div class="listing__content">
		              <div class="listing__header">
		                <div class="listing__header-primary">
		                  <h3 class="listing__title"><a href="<?php echo $locLink ?>"><?php echo $field_data['location_name'] ?></a></h3>
		                  <h3 class="listing__title"><a href="<?php echo $locLink ?>"><?php echo $soldText ?></a></h3>
		                  <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> <?php echo $field_data['location_location'] ?></p>
		                </div>
		              </div><!-- .listing__header -->
		            </div><!-- .listing-content -->
		          </div><!-- .item-grid__content-container -->
		        </div><!-- .listing -->
		      </div><!-- .col --> 
		  <?php }
	    }
	}?>



