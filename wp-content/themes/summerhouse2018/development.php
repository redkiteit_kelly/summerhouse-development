<?php /* Template Name: Development Template */ ?>
<?php get_header(); ?>
<section class="main-listing">
  <section class="map-container">
	<?php 
		if ( have_posts() ) : while ( have_posts() ) : the_post();
			get_template_part( 'content-map', get_post_format() );  
		endwhile; endif;
	?>
  </section><!-- .map-container -->

  <section class="listing-search">
    <div class="container">
    	<h2><?php the_title(); ?></h2>
    </div><!-- .container -->
  </section><!-- .listing-search -->

  <section class="main-listing__grid">
    <div class="container">
	    <?php custom_breadcrumbs(); ?>
      <div class="row">
	    <?php 
		if ( have_posts() ) : while ( have_posts() ) : the_post();			  	
			get_template_part( 'content-property-listing', get_post_format() );
		endwhile; endif; 
		?>
     <!--  <div class="item-grid--centered">
        <ul class="pagination">
          <li class="pagination__item">
            <a href="#" class="pagination__link pagination__link_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
          </li>
          <li class="pagination__item"><a href="#" class="pagination__link pagination__link--selected">1</a></li>
          <li class="pagination__item"><a href="#" class="pagination__link">2</a></li>
          <li class="pagination__item"><a href="#" class="pagination__link">3</a></li>
          <li class="pagination__item"><a href="#" class="pagination__link pagination__link_next"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
        </ul>pagination 
      </div>-->
    </div><!-- .container -->
  </section><!-- .item-grid-3 -->
</section><!-- .main-listing -->
<?php get_footer(); ?>