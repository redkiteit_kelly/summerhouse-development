<?php 
				$sim_props = CFS()->get('similar_properties');
				if(isset($sim_props)){?>
					      <section class="widget widget--white widget--padding-20">
			              	<h3 class="widget__title">Available Homes</h3>
					<?php
					foreach ($sim_props as $prop_id) {
					    $the_prop = get_post($prop_id);
						$field_data = CFS()->get(false, $prop_id);
						$propLink = get_permalink($prop_id);
					    ?>
				                <div class="similar-home">
				                  <a href="<?php echo $propLink ?>">
				                    <div class="similar-home__image">
				                      <div class="similar-home__overlay"></div><!-- .similar-home__overlay -->
									  <?php
									  $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($prop_id), array(223,137));
						              if(isset($feat_image)){?>
							              <img src="<?php echo $feat_image[0] ?>" alt="<?php echo $the_prop->post_title ?>">
						              <?php } ?>
				                      <!--<span class="similar-home__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>-->
				                    </div><!-- .similar-home__image -->
				                    <div class="similar-home__content">
				                      <h3 class="similar-home__title"><?php echo $the_prop->post_title ?></h3>
				                      <span class="similar-home__price"><?php echo $field_data['property_price'] ?></span>
				                    </div><!-- .similar-home__content -->
				                  </a>
				                </div><!-- .similar-home -->
            
				<?php } ?>
							 </section><!-- .widget -->
		<?php } ?> 
