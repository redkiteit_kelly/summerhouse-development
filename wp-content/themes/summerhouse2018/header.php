<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>  class="no-js">
<head>
	<!-- Basic need -->
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="Summerhouse Development Property Sales">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
	<!-- Mobile specific meta -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="format-detection" content="telephone-no">
	<?php wp_head(); ?>
	<!-- External Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:700,800|Roboto:400,500,700" rel="stylesheet"> 

	<!-- CSS files 
	<link rel="stylesheet" href="css/plugins.css">
	<link rel="stylesheet" href="css/style.css">-->
</head>

<body>

<header class="header header--mauve header--top">
	<div class="container">
		<div class="topbar">
			<ul class="topbar__contact">
				<li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:01325488488" class="topbar__link">01325 488488</a></li>
				<li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:sales@summerhousedevelopment.co.uk" class="topbar__link">sales@summerhousedevelopment.co.uk</a></li>
			</ul><!-- .topbar__contact -->

			<!--<ul class="topbar__social">
				<li><a href="#" class="topbar__link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#" class="topbar__link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>--><!-- .topbar__social -->
		</div><!-- .topbar -->

		<div class="header__main">
			<div class="header__logo">
				<h1 class="screen-reader-text">Summerhouse Development Property Sales</h1>
				<img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Summerhouse Development logo">
			</div><!-- .header__main -->

			<div class="nav-mobile">
				<a href="#" class="nav-toggle">
					<span></span>
				</a><!-- .nav-toggle -->
			</div><!-- .nav-mobile -->
			<div class="header__menu header__menu--v1">
				<?php /*wp_nav_menu( array(
					'theme_location' => 'primary', // Setting up the location for the main-menu, Main Navigation.
					'menu_id'         => '',
					'menu_class'	=> 'header__nav',
					'container' => '',
					'walker' => new wp_bootstrap_navwalker(),
					'fallback_cb' => 'wp_page_menu' //if wp_nav_menu is unavailable, WordPress displays wp_page_menu function, which displays the pages of your blog.
					)
				);*/ ?>
				<ul class="header__nav">
					<li class="header__nav-item"><a href="/" class="header__nav-link">Home</a></li>
					<li class="header__nav-item"><a href="/about-us" class="header__nav-link">About Us</a></li>
					<li class="header__nav-item">
						<a href="#" class="header__nav-link">Properties</a>
						<ul>
							<li><a href="/the-banning/">The Banning</a></li>
							<li><a href="/the-brenchley/">The Brenchley</a></li>
							<li><a href="/the-carlton/">The Carlton</a></li>
							<li><a href="/the-harewood/">The Harewood</a></li>
						</ul>
					</li>
					<li class="header__nav-item"><a href="/site-plan" class="header__nav-link">Site Plan</a></li>
					<li class="header__nav-item"><a href="/help-to-buy" class="header__nav-link">Help to Buy</a></li>
					<li class="header__nav-item"><a href="/contact-us" class="header__nav-link">Contact Us</a></li>
				</ul><!-- .header__nav -->
			</div><!-- .header__menu -->
		</div><!-- .header__main -->
	</div><!-- .container -->
</header><!-- .header -->