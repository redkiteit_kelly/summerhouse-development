<footer class="footer">
	<div class="footer__links">
		<div class="container">
			<div class="row">
				<div class="col-md-4 footer__links-single">
					<h3 class="footer__title">Contact Us</h3>
					<ul class="footer__list">
						<li><span class="footer--highlighted">Address:</span><br/>81 Bondgate<br/>Darlington<br/>County Durham<br/>DL3 7JT</li>
						<li><span class="footer--highlighted">Email:</span> <a href="mailto:sales@summerhousedevelopment.co.uk">sales@summerhousedevelopment.co.uk</a></li>
						<li><span class="footer--highlighted">Phone:</span> <a href="tel:+01325488488">01325 488 488</a></li>
					</ul>					
				</div>
				<div class="col-md-4 footer__links-single">
					<div style="text-align:center;margin:0 15px;">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/labc-awards.gif" alt="LABC Building Excellence Awards" /><br/><a href="http://www.marcusalderson.co.uk" title="Marcus Alderson Estate Agents" target="_blank"><img src="/wp-content/themes/summerhouse2018/images/marcus-alderson-logo.png" alt="" style="margin:0 auto;" /></a><br/><a href="http://www.forceshomes.com" title="Foreces Homes" target="_blank"><img src="/wp-content/themes/summerhouse2018/images/forces-homes-logo.png" alt="" style="margin:0 auto;" /></a>
					</div>
				</div>
				<div class="col-md-4 footer__links-single"></div>
					<h3 class="footer__title">Summerhouse Development</h3>
					<ul class="footer__list">
						<li><a href="/">Home</a></li>
						<li><a href="/about">About Us</a></li>
						<li><a href="/help-to-buy">Help to Buy scheme</a></li>
						<li><a href="/contact">Contact us</a></li>
						<li><a href="/terms-of-use">Terms of Use</a></li>
						<li><a href="/privacy-cookies">Privacy Policy / Cookies</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer__copyright">
		<div class="container">
			<div class="footer__copyright-inner">
				<p class="footer__copyright-desc">
					&copy; <?php echo date('Y');?> <span class="footer--highlighted">Summerhouse Development</span>. All Right Reserved.
				</p>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
</body>
</html>