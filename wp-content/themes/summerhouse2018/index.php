<?php get_header(); ?>
<section class="about-us">
	<div class="container">
		<div class="about-us__main">
	    	<div class="row">
	        	<main class="col-md-8 col-md-main">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Widget') ){ }	?>
					<?php if ($new_query->have_posts()) : ?>			
						<?php while ($new_query->have_posts()) : $new_query->the_post(); ?>
								<article <?php post_class('post_box'); ?>>
							<header>
								<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
							</header>
							<p><?php $excerpt = get_the_excerpt(); print $excerpt; ?> <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title(); ?>">Read More...</a></p>
							<div class="clear"></div>
							<div class="quote"></div>
						</article>
						<?php endwhile;
						wp_reset_postdata(); ?>
					<?php else : ?>
						<div class="headline">
							<h2 class="entry-title">Error 404 - Not Found</h2>
						</div>
						<div class="entry">
							<p>Sorry, but you are looking for something that isn't here.</p>
							<?php get_search_form(); ?>
						</div>
					</div><!--/404-->
		
					<?php endif; ?>
				</main>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>